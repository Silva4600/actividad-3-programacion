﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Technologia_de_Videojuegos_Actividad_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Invertir un array. numero 7//
            #region Actividad 1
            /*
            int[] arrayInicial = { 1, 2, 3, 4, 5 };
            int[] arrayInvertido = new int[5];

            for(int i = 0; i<arrayInicial.Length; i ++)
            {
                arrayInvertido[i] = arrayInicial[arrayInicial.Length - i - 1];
            }

            for( int i = 0; i<arrayInicial.Length; i++)
            {
                Console.Write(arrayInvertido[i] + "-");


            }

            Console.ReadKey();

            //numero 1//
            int sum = 0;
            int contador = 2;
            while(contador < 500)
            {
                if (contador % 2 == 0)
                {
                    sum = sum + contador;
                }

                contador++;
             
            }
            Console.WriteLine(sum);
            Console.ReadKey();
            */

            #endregion

            #region actividad 2
            /* actividad 2
static void Main(string[] args)
{

    for (int i = 1; i<100; i++)
    {
        if ( i % 3 != 0 && i % 5 != 0)
        {
            Console.WriteLine(i);
        }
    }

    

}
*/

            #endregion

            #region actividad 3
            /* actividad 3
                    static void Main(string[] args)
                    {
                        Console.WriteLine("Introduce nota");
                        int x = System.Convert.ToInt16(Console.ReadLine());


                        switch (x)
                        {
                            case 5:
                                Console.WriteLine(x + " -Suficiente");
                                break;
                            case 6:
                                Console.WriteLine(x + " -Suficiente");
                                break;
                            case 7:
                                Console.WriteLine(x + " -Bien");
                                break;

                            case 8:
                                Console.WriteLine(x + " -Bien");
                                break;
                            case 9:
                                Console.WriteLine(x + " -Sobresaliente");
                                break;
                            case 10:
                                Console.WriteLine(x + " -Matricula de Honor");
                                    break;

                        }
                        if (x < 5) {
                            Console.WriteLine(x + " -Suspenso");

                        }
                        Console.ReadKey();
                    }
                */
            #endregion
            
            #region actividad 4
            /* actividad 4
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce año - en formato XXXX");
            int x = System.Convert.ToInt16(Console.ReadLine());

            if (x % 4 == 0)
            {
                Console.WriteLine(x + " -año bisiesto");

            }
            else
            {
                Console.WriteLine(x + " -año no bisiesto");
            }

            Console.ReadKey();

        }

    */


            #endregion

            #region actividad 5 //funciona


            


            int[] arrayNum = new int[50];

            Random rnd = new Random();
            for (int i = 0; i < 50; i++) //numeros lanzados metidos en array
            {
                
                int dice = rnd.Next(1, 7);
                arrayNum[i] = dice;

            }

            for (int j = 1; j <= 6; j++)
            {



                int numCheck = j;

                int count = 0;
                for (int i = 0; i < arrayNum.Length; i++)
                {
                    if (arrayNum[i] == numCheck)
                    {
                        count++;
                    }

                }
                Console.WriteLine("El numero " + numCheck.ToString() + " se ha repetido " + count.ToString() + " veces en los 50 lanzamientos");



            }

    */


            #endregion

            #region actividad 6 //funciona
            /*
            Console.WriteLine("Size de array?");
            int size = System.Convert.ToInt16(Console.ReadLine());
            int[] returnedArray = getRamdomArray(size, 1, 9);

            for(int i = 0; i < size; i++)
            {
                Console.WriteLine("Posicion " +i+" del array => "+returnedArray[i].ToString());
            }

            */
            #endregion

            #region actividad 7 //funciona
            /*
            int[] arrayQueInvertir = new int[] { 1, 2, 3, 4, 5 };
            int[] arrayInvertida = invertArray(arrayQueInvertir);
            
            Console.Write("Array de entrada => [");
            for (int i=0; i < arrayQueInvertir.Length; i++)
            {
                
                if(i== arrayQueInvertir.Length - 1)
                {
                    Console.Write(arrayQueInvertir[i].ToString() + "]"); 
                }
                else
                {
                    Console.Write(arrayQueInvertir[i].ToString() + ", ");
                }
            }
            Console.WriteLine();
            Console.Write("Array de salida => [");
            for (int i = 0; i < arrayInvertida.Length; i++)
            {

                if (i == arrayInvertida.Length - 1)
                {
                    Console.Write(arrayInvertida[i].ToString() + "]");
                }
                else
                {
                    Console.Write(arrayInvertida[i].ToString() + ", ");
                }
            }

    */
            #endregion


            #region actividad 8
            /* Actividad 8
                   static void Main(string[] args)
                   {
                       int[] numArray = new int[] { 1, 2, 3 };
                       Console.WriteLine(basicArrayCalculator(numArray, '-').ToString());
                       Console.ReadKey();
                   }
                   public static int basicArrayCalculator(int[] myArray, char operacion)
                   {
                       switch (operacion)
                       {
                           case '+':
                               int res = 0;
                               for (int i = 0; i < myArray.Length; i++)
                               {
                                   res += myArray[i];
                               }
                               return res;
                               break;
                           case '*':
                               int res2 = 1;
                               for (int i = 0; i < myArray.Length; i++)
                               {
                                   res2 = res2 * myArray[i];
                               }
                               return res2;
                               break;
                           case '-':
                               int res3 = myArray[0];
                               for (int i = 1; i < myArray.Length; i++)
                               {
                                   res3 -= myArray[i];
                               }
                               return res3;
                               break;
                           default:
                               break;
                       }
                       return 0; //base case nunca se utiliza


                   }
                   */
            #endregion

            #region actividad 9 funciona
            /*
             Console.WriteLine("Que numero encontrar?");
             int numToFind = System.Convert.ToInt16(Console.ReadLine());


             int[,] biArray = new int[,] { { 1, 4 }, { 4, 8 },{ 6, 7 } };

             bool result = findIn2DArray(biArray, numToFind);

             Console.WriteLine(result);
             */
            #endregion

            #region actividad 10
            /* actividad 10
   static void Main(string[] args)
   {
       int[] numarr = new int[25];
       numarr[0] = 1;
       numarr[1] = 1;
       for(int i = 2; i < 24; i++)
       {
           numarr[i] = numarr[i - 2] + numarr[i - 1];

       }
       for (int i = 0; i < 24; i++)
       {
           Console.WriteLine(numarr[i]);
       }
       Console.ReadKey();
   }
      */

            #endregion

            Console.ReadKey();//NUNCA BORRAR
        }


        //USADO EN ACTIVIDAD 6
        static int[] getRamdomArray(int size, int minNum, int maxNum)
        {
            int[] arrayNum = new int[size];

            Random rnd = new Random();
            for (int i = 0; i < size; i++) //numeros lanzados metidos en array
            {

                int randNum = rnd.Next(minNum, maxNum+1);//mas uno asi incluye el NUMERO MAXNUM ya que la funcion rand es hasta (no incluyendo) el ultimo numero
                arrayNum[i] = randNum;

            }

            

            return arrayNum;
        }


        //USADO EN ACTIVIDAD 7
        static int[] invertArray(int[] inputArray)
        {
            int size = inputArray.Length;
            int[] result = new int[size];

            int count = 0;
            for(int i = size-1; i >=0; i--)
            {
                result[count] = inputArray[i];
                
                count++;
            }
            



            return result;

        }

        //USADO EN ACTIVIDAD 9
        static bool findIn2DArray(int[,] biArray, int numToFind)
        {
          
           
            bool result = false;
            for(int i=0; i < (biArray.Length / 2); i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    int number = biArray[i,j];
                    if(number == numToFind)
                    {
                        result = true;
                        break;
                    }

                }
            }

            return result;
        }






    }

    
}
